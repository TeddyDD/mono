# fish-mono

No bells and whistles, true minimal prompt for the fish shell.

![screen](screen1.png)
![screen](screen2.png)

## Installation

With Fisher:

```
fisher add fishpkg/fish-prompt-mono
```

## Features

* Git
    * Clean
    * Dirty / Touched
    * Staged
    * Staged + Dirty
    * Stashed
    * Unpushed commits (ahead)
    * Unpulled commits (behind)
    * Unpulled and unpushed commits (diverged)
    * Detached HEAD
    * Branch name
* $status
* $CMD_DURATION
* $VIRTUAL_ENV
* $status in context
* Background jobs
* Superpowers (sudo)
* Host information

## License

Since Mono and its dependecies was removed from Github I had to restore this code
from my dotfiles. Therefore I'm not sure who is the author or
what was oryginal license of the plugins.

From Github forks of Mono I see that autho was Jorge Bucaran and Mono was licensed under Unlicense

```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to http://unlicense.org

```

When it comes to other dependecies

```
fishpkg/fish-git-util
fishpkg/fish-pwd-is-home
fishpkg/fish-pwd-info
fishpkg/fish-host-info
fishpkg/fish-last-job-id
fishpkg/fish-humanize-duration
```

I can't find information who wrote them and how they were licensed. If you are an
author of any of this and you want me to add credits/proper license/remove your code
form this repo - feel free to contact me. Until then I'm assuming they are Unlicense as well.